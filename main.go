package main

import (
	"encoding/json"
	"fmt"
	htmlpkg "golang.org/x/net/html"
	"io"
	"net/http"
	"strings"
)

var url = "https://www.codewars.com/kata/search/"

func main() {
	languages := Parse()
	dd(languages)
}

func Parse() []string {
	html, err := getHtml()
	if err != nil {
		fmt.Println(err.Error())
	}
	tokenizer := tokenize(html)
	languages := findLanguages(tokenizer)
	return languages
}

func tokenize(html string) *htmlpkg.Tokenizer {
	reader := strings.NewReader(html)
	tokenizer := htmlpkg.NewTokenizer(reader)
	return tokenizer
}

func getHtml() (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err.Error())
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			panic(err.Error())
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)

	return string(body), err
}

func findLanguages(tokenizer *htmlpkg.Tokenizer) []string {
	result := make([]string, 0)
	inSelects := false
	tokenCurrent := htmlpkg.Token{}
	for {

		tokenType := tokenizer.Next()
		tokenCurrent = tokenizer.Token()
		switch tokenType {
		case htmlpkg.StartTagToken:
			if tokenCurrent.Data != "sl-select" {
				continue
			}
			if inAttributes(tokenCurrent.Attr, "id", "language_filter") {
				inSelects = true
			}
		case htmlpkg.EndTagToken:
			if tokenCurrent.Data == "sl-select" {
				inSelects = false
			}
		case htmlpkg.TextToken:
			if inSelects {
				result = append(result, tokenCurrent.Data)
			}
		case htmlpkg.ErrorToken:
			return result
		}

	}
}

func inAttributes(attributes []htmlpkg.Attribute, key string, value string) bool {
	for _, attribute := range attributes {
		if attribute.Key == key && attribute.Val == value {
			return true
		}
	}
	return false
}

func dd(values []string) {
	for _, value := range values {
		marshal, _ := json.MarshalIndent(value, "", "	")
		fmt.Println(string(marshal))
	}
}
