package main

import (
	"testing"
)

func TestParse(t *testing.T) {
	results := Parse()
	println(len(results))
	if len(results) == 0 {
		t.Errorf("Empty list")
		return
	}
	first := results[0]
	last := results[len(results)-1]
	if first != "All" || last != "VB (Beta)" {
		t.Errorf("Wrong languages. %s, %s", first, last)
		return
	}
}
